# @see https://mherman.org/blog/dockerizing-an-angular-app/
# base image
FROM node:12.2.0

LABEL maintainer="Pablo Lionel Benitez - pablo.lionel.b@gmail.com"

# Create app directory (with user `node`)
RUN mkdir /app

# set working directory
WORKDIR /home/app

# install chrome for protractor tests
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' && \
    apt-get update && apt-get install -yq google-chrome-stable

# add `/app/node_modules/.bin` to $PATH
ENV PATH /home/app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package*.json .
RUN npm install

# add app
COPY . .

# start app
CMD ["npm", "start"]
